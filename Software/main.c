/*
* OnOffPowerSwitcher.c
*
* Created: 2018-05-27 20:11:09
* Author : Kamil
* email  : siecinski.kamil@gmail.com
*/


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

#define mCountedTime_Hours		(5)
#define mCountedTime_Minutes	(5)
#define mCountedTime_Seconds	(5)

#define mCountedStep_Seconds	(2)

volatile uint8_t u8Hours = 0;
volatile uint8_t u8Minutes = 0;
volatile uint8_t u8Seconds = 0;


void ControlPin_Init(void)
{
	/* set pin 3 of port B as output */
	DDRB |= (1<<PINB3);
	/* set state high to pin 3 of port B to turn off Transistor */
	PORTB |= (1<<PINB3);
}

void Timer_Init()
{
	/*
	F_CPU = 128 000
	prescaler = 1024
	Timer clock = 128000/1024 = 125
	Timer increment value by 125 every second.
	*/
	TCCR0A |= (1 << WGM01);					//CTC (Mode 2).
	TCCR0B |= (1 << CS00) | (1 << CS02);	//Set prescaler to 1024.
	OCR0A = 250;							//Count 2 seconds.
	TIMSK0 |= (1 << OCIE0A);				//Generate interrupt when timer0 reach OCR0A value. (every 2 seconds).
}
void LowPowerMode_Init()
{
	PRR |= (1 << PRADC);	//Turn off ADC in sleep mode
							//Mode Idle selected
	MCUCR |= (1 << SE);		//Sleep enable
}

int main(void)
{
	cli();	//disable interrupts for initialization
	/* Replace with your application code */
	ControlPin_Init();
	Timer_Init();
	LowPowerMode_Init();
	
	sei();	//enable interrupts after initialization
	sleep_cpu();
	while (1)
	{
		if(u8Hours >= mCountedTime_Hours && u8Minutes >= mCountedTime_Minutes && u8Seconds >= mCountedTime_Seconds )
		{
			u8Seconds = 0;
			u8Minutes = 0;
			u8Hours = 0;
			PORTB &= ~(1 << PINB3); //set level low to turn on transistor switch
		}
	sleep_cpu();
	}
}

ISR(TIM0_COMPA_vect)
{
	cli();	// disable interrupts
	u8Seconds += mCountedStep_Seconds;
	PORTB |= (1 << PINB3); //set level to high to turn off transistor switch. 
	if(u8Seconds >= 60)
	{
		u8Minutes++;
		u8Seconds = 0;
	}
	if(u8Minutes >= 60)
	{
		u8Hours++;
		u8Minutes = 0;
	}
	sei();	//enable interrupts
}